# gjj-dashboard
### Dashboard for Gitlab merge requests, Jenkins pipelines, Jira tasks and other stuff

[![screenshot](https://gitlab.com/hellobvious/gjj/raw/master/screenshot.jpg)](https://gitlab.com/hellobvious/gjj)


## REQUIREMENTS

### Python 3+

### Conky 1.9+

Conky is a powerful tool used to display information on desktop.
* [Conky home](https://github.com/brndnmtthws/conky)

> To install Conky on Debian simply type:
`sudo apt install conky-all`

## LAUNCH

1. Add **token**, **user_name** and **url(s)** to **`gjj-config.yaml`**

2. Start/end app via **`gjj.sh`**

> Make the .sh file executable by `chmod 755` (sets permits to executable)
or `chmod a+x` (adds executable to permits)

## API TOKENS

Add token info to `api_token` key in `gjj-config.yaml` file.

Token addresses:

* Gitlab
`https://gitlab.server.address/profile/personal_access_tokens`
    
   > **Token with `api` lvl access is required**

* Jenkins
`https://jenkins.server.address/user/{user_name}/configure`

* Jira
`https://id.atlassian.com/manage/api-tokens`

#### Token using config file only

* use raw token value

    i.e. `"api_token": "1234qwerASDF0987"`

#### Token using environment variable

* save your api token as an environment variable

    (i.e. by adding line `NAME_OF_ENV_VAR=api_key_value` to `/etc/environment` and re-logging)

* use format: `$ENV{`**`NAME_OF_ENV_VAR`**`}`

    i.e. `"api_token": "$ENV{GJJ_GITLAB_API_TOKEN}"`

> **`KEEP YOUR TOKENS PRIVATE!`**

## CONFIG

Configuration file: `gjj-config.yaml`

#### Gitlab module

Gitlab module displays merge request info. Url format is:

`https://gitlab.server.address/api/v4/projects/{}/merge_requests?state=opened`

> **note:** `{}` will be replaced by id from config field `gitlab.projects.id`

#### Jenkins module

Jenkins module displays pipeline info (a list of jobs). Url format is:

`https://jenkins.server.address/view/name%20of%20pipeline%20view`

#### Jira module

Jira module displays task info. Url format is:

`https://yourserver.atlassian.net/rest/api/2/search?fields=issuetype,parent,status,summary&jql=`

* **jql query is used, and is main tool for task filtering**
      
    config field: `jira.setup.jql_query`
    
* url param `fields` is used to project only those fields in response,
    which would otherwise contain a lot more information
    remove it to get full response in `out/out-jira.json`

#### Info module

Module based on regular Conky features. Feel free to experiment.

## Settings

Settings file: `gjj-settings.yaml`

Most settings are based on Conky. Best described here:
[conky variables](https://github.com/brndnmtthws/conky/wiki/Configuration-Variables)
and here:
[conky settings](http://conky.sourceforge.net/config_settings.html).

#### Update interval

- `global_update_interval` refreshing all window elements
(includes executing all commands from .rc file). Default is 5 seconds.

- `update_interval` overrides global interval and allows to define
longer refresh periods for specific commands (like refreshing Jira tasks).
Default is 600s for Gitlab and Jira and 10s for Jenkins.

> note: `update_interval` **has to be >=** `global_update_interval`

#### Char limit

- `char_limit` Text will be truncated to this length.
    Use to adjust window width.
    
    Used in:
    - Gitlab merge request title (1 line is shown)
    - Jenkins project and job names (1 line is shown)
    (For description fields: 'parameters', 'cause' and 'culprits' 2 lines are shown, limit applies to one line)
    - Jira summary (2 lines are shown, limit applies to one line)

### Window types

* config field: `module.setup.window_type`
* values: `panel`, `normal`, `desktop`, `dock`, `override`

### Fonts

* config field: `module.setup.font`
* example: `MathJax_Caligraphic:size=10`

To get available fonts on Debian:

`fc-list` or `fc-list | grep 'Mono'`

.../fonts/MathJax_Caligraphic-Regular.otf: `MathJax_Caligraphic`:style=Regular

## OUTPUT

Request(s) from last execution are stored in `out/out-module_name.json`

## LICENSE
***This is a non-profit project.***

*All content used in this project including, but not limited to,
text, image, audio, video
is either self-made or used without permission, 
with all rights reserved to respective owners, 
and no challenge to their status intended.*

This software is under [WTFPL](http://www.wtfpl.net/txt/copying/)

[![screenshot](http://www.wtfpl.net/wp-content/uploads/2012/12/logo-220x1601.png)](http://www.wtfpl.net/)

> Logotypes of Gitlab, Jenkins and Jira belong to their respective owners.
No challenge to their status intended.

<hr/>
author: kuduacz.pl
<hr/>

### Credits

Background art used for screenshot:

https://www.robertogattoart.com/projects/qX2Ny
