#!/bin/bash

killall conky && echo '** Conky processes found. Ending gjj-dashboard. Bye.' && exit 0

echo '** Starting gjj-dashboard..' && cd `dirname $0`/py/ && python3 gjj-conf.py
