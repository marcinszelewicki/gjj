#!usr/bin/env python3
# coding=utf-8

import json
import os
from base64 import b64encode
from operator import itemgetter
from time import time

import urllib3
import yaml

_api_url_ = '/api/json?tree=name,jobs[name,color,lastBuild[number,builtOn,' \
            'actions[causes[shortDescription],parameters[name,value]],' \
            'estimatedDuration,timestamp,result,culprits[fullName,absoluteUrl,name]]]'

_root_path_ = os.path.dirname(os.getcwd())


def read_yaml(file_):
    with open('{}/{}.yaml'.format(_root_path_, file_)) as cfg:
        yaml_ = yaml.load(cfg)
        cfg.close()
    return yaml_


_config_ = read_yaml('gjj-config')['jenkins']
_yaml_ = read_yaml('gjj-settings')
_settings_ = _yaml_['jenkins']
_colors_ = _yaml_['colors']
_char_limit_ = _settings_['char_limit']
_out_ = []


def encode_token(user_mail, token):
    api_token = "{}:{}".format(user_mail, token)
    return b64encode(bytes(api_token, "utf-8"))


def get_token():
    token = _config_['api_token']
    if token.startswith('$ENV{'):
        env = token[5:len(token) - 1]
        return os.environ[env.strip()]

    return token


def get_response(url_, encoded_token_):
    try:
        http_ = urllib3.PoolManager()
        resp_ = http_.request('GET', url_,
                              headers={
                                  'Authorization': "Basic " + encoded_token_.decode("ascii"),
                                  'Content-Type': "application/json"
                              })
        return json.loads(resp_.data.decode('utf-8'))

    except Exception as e:
        print('Error during request\n{0}\n{1}'.format(url_, str(e)))
        return None


def out_to_file():
    with open('{}/out/out-jenkins.json'.format(_root_path_), 'w') as out_file:
        out_file.write(json.dumps(_out_))
    out_file.close()


def get_title():
    title = _config_['title']
    title = title if title else "Jenkins"
    return "{}{}$color".format(_colors_['main'], title)


def get_project_name(project_, response_):
    return project_['displayed_name'][:_char_limit_] if project_['displayed_name'] \
        else response_['name'][:_char_limit_]


def get_culprits(last_job):
    ret = ''
    for culprit in last_job['culprits']:
        ret += extract_user_name(culprit) + ' '

    return '\nCulprits: ' + ret if ret else ret


def extract_user_name(culprit_):
    url_ = culprit_['absoluteUrl']
    return url_[url_.rfind('/') + 1:]


def add_culprits(color_, job_):
    clr_ = _colors_[color_]
    return clr_, break_line(get_culprits(job_['lastBuild']))


def get_cause(last_job):
    for action in last_job['actions']:
        get = action.get('_class')
        if get and get.find('hudson.model.CauseAction') > -1:
            return action.get('causes')[0]['shortDescription']

    return None


def get_parameters(job_):
    ret = ''
    for action in job_['actions']:
        get = action.get('_class')
        if get and get.find('hudson.model.ParametersAction') > -1:
            for param in action.get('parameters'):
                ret += param.get('name') + ':' + param.get('value') + ' '

    return '\n' + break_line(ret) if ret else ret


def get_time_spent(job_):
    estimate = int(job_['estimatedDuration'] / 1000)
    timestamp = int(job_['timestamp'] / 1000)
    duration = int(time()) - timestamp
    progress = int(duration * 100 / estimate)
    return '\n{} / {} [{}{}$color]'.format(
        parse_time(duration), parse_time(estimate), _colors_['main'], str(progress) + '%')


def parse_time(seconds):
    return '{}m{}s'.format(int(seconds / 60), seconds % 60)


def pad(indent_, string_):
    return ''.ljust(indent_) + string_


def color(color_, text_):
    return '{}{}$color'.format(_colors_[color_], text_)


def break_line(input_):
    ret = ''
    return ret + input_ if len(input_) <= _char_limit_ \
        else ret + input_[:_char_limit_] + '\n' + ret + input_[_char_limit_:2 * _char_limit_ - 1]


def prepend_lines(lines_):
    return lines_.replace("\n", "\n \# ")


def get_header(jobs_, json_response_, project_):
    text_ = "\n\n" + color('yellow', get_project_name(project_, json_response_))
    first_ = jobs_[0]['lastBuild']
    lines_ = '\n' + break_line(get_cause(first_)) + '\non: ' + first_['builtOn'] + get_parameters(first_)
    text_ += prepend_lines(lines_)
    return text_


def get_text(project_, json_response_):
    jobs_ = json_response_['jobs']

    text_ = get_header(jobs_, json_response_, project_)

    for job in jobs_:
        name_ = '| ' + job['name'][:_char_limit_]
        color_ = job['color']
        clr_ = _colors_['green']
        prefix_ = ''
        comment_ = ''
        last = job['lastBuild']

        if color_.find('_anime') > -1:
            clr_ = _colors_['blue']
            prefix_ = _colors_['main'] + ' -> ' + '$color'
            name_ = job['name'][:_char_limit_ - 4]
            comment_ = get_parameters(last) + get_time_spent(last)
        if color_.find('yellow') > -1:
            clr_, comment_ = add_culprits('yellow', job)
        if color_.find('red') > -1:
            clr_, comment_ = add_culprits('red', job)
        if last['result'] and last['result'].find('ABORTED') > -1:
            clr_, comment_ = add_culprits('gray', job)

        text_ = text_ + "\n{}{}{}$color{}".format(prefix_, clr_, name_, prepend_lines(comment_))

    return text_


def process(project_, token_):
    response_ = get_response(project_['view_url'] + _api_url_, token_)
    _out_.append(response_)
    return get_text(project_, response_)


def run_script():
    token_ = encode_token(_config_['user_name'], get_token())
    projects_ = sorted(_config_['pipelines'], key=itemgetter('pos'), reverse=True)
    text = get_title()

    for project in projects_:
        text += process(project, token_)

    print(text)
    out_to_file()


run_script()
