#!usr/bin/env python3
# coding=utf-8

import json
import os
from operator import itemgetter

import urllib3
import yaml

_root_path_ = os.path.dirname(os.getcwd())


def read_yaml(file_):
    with open('{}/{}.yaml'.format(_root_path_, file_)) as cfg:
        yaml_ = yaml.load(cfg)
        cfg.close()
    return yaml_


_config_ = read_yaml('gjj-config')['gitlab']
_yaml_ = read_yaml('gjj-settings')
_settings_ = _yaml_['gitlab']
_colors_ = _yaml_['colors']
_char_limit_ = _settings_['char_limit']
_out_ = []


def get_response(project_id, token_):
    try:

        url = _config_['mr_url'].format(str(project_id))
        http = urllib3.PoolManager()
        response = http.request('GET', url, headers={'Private-Token': token_})
        return json.loads(response.data.decode('utf-8'))

    except Exception as e:
        print('Error during request\n{0}\n{1}'.format(url, str(e)))
        return None


def get_token():
    token = _config_['api_token']
    if token.startswith('$ENV{'):
        env = token[5:len(token) - 1]
        return os.environ[env.strip()]
    return token


def out_to_file():
    with open(_root_path_ + '/out/out-gitlab.json', 'w') as out_file:
        out_file.write(json.dumps(_out_))
    out_file.close()


def color(text_, color_):
    return '{}{}$color'.format(_colors_[color_], text_)


def get_title():
    title_ = _config_['title']
    title_ = title_ if title_ else "Gitlab"
    return color(title_ + '\n', 'main')


def get_project_name(proj_):
    name_ = proj_['displayed_name']
    return '\n{}\n'.format(color(name_, 'yellow')) if name_ else '\n'


def get_text(json_response, title):
    for mr in json_response:
        mr_title_ = color(mr["title"][:_char_limit_], 'blue')
        author_name_ = ''.ljust(3) + mr["author"]["name"][:27]
        status_ = color('MERGE OK', 'green') if mr["merge_status"] == "can_be_merged" else color('CONFLICT', 'red')

        discussion_count_ = mr["user_notes_count"]
        discussion_color_ = 'main' if discussion_count_ == 0 else 'gray'
        discussion_count_text_ = 'discussion:' + str(discussion_count_)
        discussion_ = color(discussion_count_text_.ljust(13), discussion_color_)

        upvotes_ = '' if mr['upvotes'] == 0 else color(_settings_['upvote_char'] + str(mr['upvotes']), 'yellow')
        downvotes_ = '' if mr['downvotes'] == 0 else color(_settings_['downvote_char'] + str(mr['downvotes']), 'red')
        created_at_ = mr["created_at"][:10]

        title = title + " {}\n{:30s}  {:24}  {:13}  {:2} {:2}  {:10}\n".format(
            mr_title_, author_name_, status_, discussion_, upvotes_, downvotes_, created_at_)

    return title


def process(project_id, title_, token_):
    rsp_ = get_response(project_id, token_)
    _out_.append(rsp_)
    return get_text(rsp_, title_)


def run_script():
    token_ = get_token()
    projects_ = sorted(_config_['projects'], key=itemgetter('pos'))
    text_ = get_title()

    for project_ in projects_:
        text_ += process(project_['id'], get_project_name(project_), token_)

    print(text_)
    out_to_file()


run_script()
