#!usr/bin/env python3
# coding=utf-8

import os
from operator import itemgetter

import yaml

_cwd_ = os.getcwd()
_root_path_ = os.path.dirname(_cwd_)


def read_yaml(file_):
    with open('{}/{}.yaml'.format(_root_path_, file_)) as cfg:
        yaml_ = yaml.load(cfg)
        cfg.close()
    return yaml_


_rc_root_ = "{}/rc/.conkyrc-".format(_root_path_)
_conky_command_pref_ = "conky -c " + _rc_root_
_settings_ = read_yaml('gjj-settings')
_config_ = read_yaml('gjj-config')


def set_rc_file(mod_name):
    with open(_rc_root_ + mod_name, 'r') as r:
        rc_file = r.readlines()
    r.close()

    setup = _settings_[mod_name]

    rc_file[0] = 'alignment {}\n'.format(setup['alignment'])
    rc_file[1] = 'xftfont {}\n'.format(setup['font'])
    rc_file[2] = 'gap_x {}\n'.format(setup['gap_x'])
    rc_file[3] = 'gap_y {}\n'.format(setup['gap_y'])
    rc_file[4] = 'own_window_type {}\n'.format(setup['window_type'])
    rc_file[5] = 'update_interval {}\n'.format(_settings_['global_update_interval'])
    rc_file[6] = 'default_color {}\n'.format(setup['default_font_color'])
    rc_file[7] = 'own_window_transparent {}\n'.format(parse_bool(setup['window_transparent']))
    rc_file[8] = 'own_window_colour {}\n'.format(setup['window_color'])

    if mod_name in 'info':
        file_ = info_command(rc_file)
    else:
        file_ = mod_command(rc_file, mod_name, setup)

    with open(_rc_root_ + mod_name, 'w') as w:
        w.writelines(file_)
    w.close()


def parse_bool(bool_):
    return 'yes' if bool_ else 'no'


def info_command(rc_file):
    index = rc_file.index("TEXT\n")
    rc_file = rc_file[:index + 1]

    lines_ = sorted(_config_['info']['lines'], key=itemgetter('pos'))

    for line in lines_:
        rc_file.append(line['text'] + '\n')

    return rc_file


def mod_command(rc_file, mod_name, setup):
    img = '${' + 'image {}/{}{}{}'.format(_root_path_, 'ico/', mod_name, '.png') + '}'
    cmd = '${' + 'execpi {} cd {}/py && python3 gjj-{}.py'.format(setup['update_interval'], _root_path_, mod_name) + '}'
    rc_file[len(rc_file) - 1] = img + cmd
    return rc_file


def run_process(command_):
    os.system(command_)


def run_script():
    mods = ['jira', 'gitlab', 'jenkins', 'info']
    for mod in mods:
        if _config_[mod]['enabled']:
            set_rc_file(mod)

    for mod in mods:
        if _config_[mod]['enabled']:
            run_process(_conky_command_pref_ + mod)


run_script()
