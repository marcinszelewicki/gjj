#!usr/bin/env python3
# coding=utf-8

import json
import os
from base64 import b64encode
from itertools import groupby
from operator import itemgetter
from urllib.parse import quote_plus

import urllib3
import yaml

_root_path_ = os.path.dirname(os.getcwd())


def read_yaml(file_):
    with open('{}/{}.yaml'.format(_root_path_, file_)) as cfg:
        yaml_ = yaml.load(cfg)
        cfg.close()
    return yaml_


_config_ = read_yaml('gjj-config')['jira']
_yaml_ = read_yaml('gjj-settings')
_settings_ = _yaml_['jira']
_colors_ = _yaml_['colors']
_char_limit_ = _settings_['char_limit']
_out_ = []


def out_to_file():
    with open(_root_path_ + '/out/out-jira.json', 'w') as out_file:
        out_file.write(json.dumps(_out_))
    out_file.close()


def encode_token(user_mail):
    api_token = "{}:{}".format(user_mail, get_token())
    return b64encode(bytes(api_token, "utf-8"))


def get_token():
    token = _config_['api_token']
    if token.startswith('$ENV{'):
        env = token[5:len(token) - 1]
        return os.environ[env.strip()]
    return token


def get_response(address_, jql_query_, encoded_token_):
    url_ = address_ + quote_plus(jql_query_)
    try:
        http_ = urllib3.PoolManager()
        resp_ = http_.request('GET', url_,
                              headers={
                                  'Authorization': "Basic " + encoded_token_.decode("ascii"),
                                  'Content-Type': "application/json"
                              })
        return json.loads(resp_.data.decode('utf-8'))

    except Exception as e:
        print('Error during request\n{0}\n{1}'.format(url_, str(e)))
        return None


def group_tasks(task_list):
    result = {key: [task for task in value] for key, value in
              groupby(task_list, lambda item: item["fields"]["parent"]["key"])}
    return result


def break_line(input_, indent_):
    padding = ''.ljust(indent_)
    return padding + input_ if len(input_) <= _char_limit_ \
        else padding + input_[:_char_limit_] + '\n ' + padding + input_[_char_limit_:2 * _char_limit_ - 1]


def color_issue(issue):
    ret = '{}' + issue + '$color'

    if any(word in issue.lower() for word in ['bug', 'merged', 'build', 'reopened']):
        return ret.format(_colors_['red'])
    if any(word in issue.lower() for word in ['request', 'review', 'new']):
        return ret.format(_colors_['yellow'])
    if any(word in issue.lower() for word in ['progress', 'workshops']):
        return ret.format(_colors_['green'])
    if any(word in issue.lower() for word in ['dev']) or issue == 'Task':
        return ret.format(_colors_['blue'])
    if any(word in issue.lower() for word in ['paused']):
        return ret.format(_colors_['gray'])

    return ret.format(_colors_['main'])


def get_title():
    if _config_['title']:
        return '{}{}$color'.format(_colors_['main'], _config_['title'])
    else:
        return ''


def get_view_name(view_):
    n = view_['displayed_name']
    return '\n\n{}{}$color'.format(_colors_['yellow'], n) if n else '\n'


def parent_text(parent_):
    return "\n{}{}$color [{}] [{}]\n {}".format(
        _colors_['main'],
        parent_['key'],
        color_issue(parent_['fields']['issuetype']['name']),
        color_issue(parent_['fields']['status']['name']),
        break_line(parent_['fields']['summary'], 0)
    )


def child_text(issue):
    fields_ = issue['fields']
    return "\n -> {}{}$color [{}] [{}]\n{}".format(
        _colors_['main'],
        issue['key'],
        color_issue(fields_['issuetype']['name']),
        color_issue(fields_['status']['name']),
        break_line(fields_['summary'], 4)
    )


def get_text(json_response_):
    parents_list = []
    children_list = []

    for issue in json_response_:
        if 'parent' in issue['fields']:
            children_list.append(issue)
        else:
            parents_list.append(issue)

    grp_children = group_tasks(children_list)
    ret = ''

    for issue in parents_list:
        ret += parent_text(issue)

    for item in grp_children:
        for i, value in enumerate(grp_children[item]):
            if i == 0:
                ret += parent_text(value['fields']['parent'])
            ret += child_text(value)

    return ret


def process(view_, token_):
    response_ = get_response(view_['search_url'], view_['jql_query'], token_)
    _out_.append(response_)
    tasks = get_text(response_['issues'])
    return get_view_name(view_) + tasks


def run_script():
    token_ = encode_token(_config_['user_name'])
    views_ = sorted(_config_['views'], key=itemgetter('pos'))
    text_ = get_title()

    for view in views_:
        text_ += process(view, token_)

    print(text_)
    out_to_file()


run_script()
